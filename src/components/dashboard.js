import React, { useEffect, useState } from 'react'
import { Navigate } from "react-router-dom";
import DashboardMenu from './dashboard-menu.component'

const Dashboard = () => {
    const authenticated = localStorage.getItem('authenticated')
    const username = localStorage.getItem('username')
    
    console.log(authenticated)
    if (!authenticated) {
        window.location.replace('http://localhost:3000/sign-in')
    }
    else{
        return (    
            <div className="App container">
                <div className="auth-wrapper">
                    <div className="auth-inner">
                        <div className='row'>
                            <div className='col'>
                                <center>
                                    <h2>Welcome { username }</h2>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};
export default Dashboard;
    