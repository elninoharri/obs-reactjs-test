import React, { Component, useEffect } from 'react'
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const Login = () => {
    const MySwal = withReactContent(Swal)
       
    const navigate = useNavigate();
    const [username, setusername] = useState("")
    const [password, setpassword] = useState("")
    const [authenticated] = useState([]);
    const users = [{ username: "admin", password: "admin" }]
    const handleSubmit = (e) => {
        e.preventDefault()
        const account = users.find((user) => user.username === username && user.password === password)

        if(account){
            MySwal.fire({
                timer: 700,
                title: <p>Please wait ...</p>,
                didOpen: () => {
                  MySwal.showLoading()
                },
              }).then(() => {
                    localStorage.setItem('authenticated', true)
                    localStorage.setItem('username', account.username)
                    // console.log(localStorage.getItem('authenticated'), localStorage.getItem('username'))
                    navigate("/dashboard");
              })   
        }
        else{
            MySwal.fire({
                timer: 700,
                title: <p>Please wait ...</p>,
                didOpen: () => {
                  // `MySwal` is a subclass of `Swal` with all the same instance & static methods
                  MySwal.showLoading()
                },
              }).then(() => {
                return MySwal.fire(<small>User doesn't exist, please check your username & password again</small>)
              })   
        }
    };
        
    return (
        <form onSubmit={handleSubmit}>
            <h3>Sign In</h3>
            <div className="mb-3">
            <label>User ID</label>
            <input
                type="username"
                className="form-control"
                placeholder="User ID"
                value={username}
                onChange={(e) => setusername(e.target.value)}

            />
            </div>
            <div className="mb-3">
            <label>Password</label>
            <input
                type="password"
                name="Password"
                className="form-control"
                placeholder="Enter password"
                onChange={(e) => setpassword(e.target.value)}
            />
            </div>
            <div className="d-grid">
            <button type="submit" value="Submit" className="btn btn-primary">
                Submit
            </button>
            </div>
        </form>
    )
}

export default Login