import React, { useEffect, useState } from 'react'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import Login from './components/login.component'
import SignUp from './components/signup.component'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const App = () => {
  const MySwal = withReactContent(Swal)
  
  const username = localStorage.getItem('username')
  const [authenticated, setAuthenticated] = useState([]);
  const handleLogout = (e) => {
    MySwal.fire({
      timer: 700,
      title: <p>Please wait ...</p>,
      didOpen: () => {
        MySwal.showLoading()
      },
    }).then(() => {
          localStorage.removeItem('authenticated')
          localStorage.removeItem('username')
          window.location.href='http://localhost:3000/';
    })   
  }
    useEffect(() => {
        const authenticated = localStorage.getItem('authenticated');
        
        if (authenticated) {
            setAuthenticated(authenticated);
        }
      }, []);
  if (authenticated.length > 0) {
    return (
      <Router>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
            <div className="container">
              <Link className="navbar-brand" to={'/dashboard'}>
                Home
              </Link>
              <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" onClick={handleLogout}>
                      Sign out
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
      </Router>
      
    )
  }   
  else{
    return (
      <Router>
        
          <nav className="navbar navbar-expand-lg navbar-light fixed-top">
            <div className="container">
              <Link className="navbar-brand" to={'/sign-in'}>
                OBS ReactJS Test
              </Link>
              <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to={'/sign-in'}>
                      Login
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={'/sign-up'}>
                      Sign up
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <div className="auth-wrapper">
            <div className="auth-inner">
              <Routes>
                <Route exact path="/" element={<Login />} />
                <Route path="/sign-in" element={<Login />} />
                <Route path="/sign-up" element={<SignUp />} />
              </Routes>
            </div>
          </div>
        
      </Router>
    )
  } 
}
export default App